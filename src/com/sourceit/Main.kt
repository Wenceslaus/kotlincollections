package com.sourceit

import com.sourceit.data.getStoreList
import com.sourceit.model.Phone
import com.sourceit.model.Store
import com.sourceit.model.discountByPrice

fun main(args: Array<String>) {


    loadData({
        println("all ok, result: $it")
        return@loadData "data"
    }, {
        println("error: $it")
    })




    val user = User("Vasya")
    user.printInfo{ println(it) }




    val list = getStoreList()
    val storeName = "citrus"

    val phoneWithMaxPrice = list.find { it.name == storeName }?.phones?.maxBy { it.price }
    println(phoneWithMaxPrice?.name ?: "not found")

    val phoneWithMinPrice = list.flatMap { it.phones }.minBy { it.price }
    println(phoneWithMinPrice?.name ?: "not found")

    val phoneWithMaxDiscount = list.find { it.name == storeName }?.phones?.
        filter { it.discount != null }?.minBy {it.discountByPrice()}
    println(phoneWithMaxDiscount?.name ?: "not found")

//    val listOfUniquePhones = list.flatMap { it.phones }.distinctBy { it.name }

    //unique phones with store names
    val listOfUniquePhones = list.flatMap {store -> store.phones.map { Pair(store.name, it) } }.distinctBy { it.second.name }

    println(listOfUniquePhones.joinToString())
}