package com.sourceit.model

fun Phone.discountByPrice() = discount?: 0 / 100 * price