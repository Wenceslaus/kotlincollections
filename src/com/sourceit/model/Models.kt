package com.sourceit.model

data class Store(
    val name: String,
    val url: String?,
    val phones: List<Phone>
)

data class Phone(
    val name: String,
    val brand: String,
    val price: Int,
    val discount: Int? = null
)