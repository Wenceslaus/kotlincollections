package com.sourceit

interface Runnable {
    fun run()
}

open class User(val name: String) {

    open fun printInfo(runnable: (String) -> Unit) {
        println(name)
        runnable.invoke(name)
    }
}

//class Admin(name: String) : User(name) {
//
//    override fun printInfo() {
//        println("$name")
//    }
//
//}
