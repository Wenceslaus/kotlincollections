package com.sourceit.data

import com.sourceit.model.Phone
import com.sourceit.model.Store

fun getStoreList() = ArrayList<Store>().apply {
    add(
        Store(
            "citrus",
            "http:site.com",
            listOf(Phone("Redmi6", "xiaomi", 3000), Phone("Iphone", "Apple", 25000))
        )
    )

    add(
        Store(
            "cactus",
            null,
            listOf(Phone("Redmi6", "xiaomi", 3000, 5), Phone("Redmi8", "xiaomi", 5000))
        )
    )
}
