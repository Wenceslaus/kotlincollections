package com.sourceit

fun loadData(result: (String) -> String, error: (String) -> Unit) {
    //
    //
    //
    //
    //
    val data: String? = null
    val serverResponse: Int = 400

    if (data == null || serverResponse != 200) {
        val data = error("Something went wrong")
    } else {
        result(data)
    }

}